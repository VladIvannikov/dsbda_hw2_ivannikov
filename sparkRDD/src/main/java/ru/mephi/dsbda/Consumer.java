package ru.mephi.dsbda;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

public class Consumer {
    public static ArrayList<String> consumer(String host, String groupid, String topic){

        // Создаем кафка консумера
        KafkaConsumer<String,String> consumer = InitKafkaConsumer(host,groupid,topic);

        ArrayList<String> getsLogs = new ArrayList<>();

        int count = 0,count_miss=0;

        // получаем сообщения, если новых сообщенийх в течении 5000мс не получили, выходим из цикла
        while(true) {
            ConsumerRecords<String,String> records = consumer.poll(1000);
            if (!records.isEmpty())
            {
                for (ConsumerRecord<String,String> record : records){
                    count++;
                    getsLogs.add(record.value());
                    //System.out.println(count+ "     :   " + record.value());
                }
            }
            else{
                count_miss++;
                if (count_miss == 5){
                    break;
                }
            }
        }
        return getsLogs;
    }

    public static KafkaConsumer<String, String> InitKafkaConsumer( String host, String groupid,String topic) {
        //создаем потребителя сообщений из кафки из topica syslogs2
        KafkaConsumer<String,String> consumer;
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers",host);
        properties.setProperty("group.id",groupid);
        properties.setProperty("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("auto.offset.reset","earliest");
        consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(Arrays.asList(topic));
        return  consumer;
    }
}
