package ru.mephi.dsbda;


// класс лог - число повторений
public class LogsWithCounts {

    String logs;
    Integer count;

    public String getLogs() {
        return logs;
    }

    public Integer getCount() {
        return count;
    }

    public LogsWithCounts(String logs, Integer count) {
        this.logs = logs;
        this.count = count;
    }
}
