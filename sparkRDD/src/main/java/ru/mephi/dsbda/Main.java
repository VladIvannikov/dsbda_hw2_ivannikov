package ru.mephi.dsbda;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.*;
import scala.Tuple2;

import java.awt.List;
import java.util.*;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.*;

public class Main {
    public static void  main (String[] args) throws Exception{



        // 1 аргумент имя bootstrap сервера, второй groupid
        if (args.length<3){
            System.out.println("Incorrect arguments");
            throw new Exception();
        }

        // задаем конфигурацию контекста
        final SparkConf sparkConf = new SparkConf().setAppName("CountPriority").setMaster("local").set("spark.cassandra.connection.host", "127.0.0.1");
        JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);


        //если второй аргумент uuid , генерируем рандомый uuid для чтения сообщений из кафки
        if (args[1].equalsIgnoreCase("UUID")){
            CountByHour(javaSparkContext,Consumer.consumer(args[0],UUID.randomUUID().toString(),args[2]));
        }
        else{
            CountByHour(javaSparkContext,Consumer.consumer(args[0],args[1],args[2]));
        }




    }



    public static int CountByHour(JavaSparkContext javaSparkContext, ArrayList<String> logs){


          JavaRDD<String> lines = javaSparkContext.parallelize(logs);



          // подсчитываем количество компинаций час-приоритет
        JavaPairRDD<String,Integer> counts = lines.flatMap((FlatMapFunction<String, String>) s -> Arrays.asList(s.split("\t")).iterator())
                .mapToPair((PairFunction<String, String, Integer>) s -> new Tuple2<String, Integer>(s,1))
                .reduceByKey((Function2<Integer, Integer, Integer>) (i1, i2) -> i1+i2);


        // вывод на экран
        counts.foreach(new VoidFunction<Tuple2<String, Integer>>() {
            @Override
            public void call(Tuple2<String, Integer> stringIntegerTuple2) throws Exception {
                System.out.println(stringIntegerTuple2._1 +"      :      " +stringIntegerTuple2._2);

            }
        });


        //открываем сессию с касандрой
        Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
        Session session = cluster.connect("java_api");


        // для каждой строки JavaPairRDD вызываем метод записи в касандру
        counts.collectAsMap().forEach((k,v) -> WriteToKassandra(session,k,v));

        //закрываем сессию с касандрой
        session.close();
        cluster.close();




        return 1;

    }


    // формируем запрос на вставку строки в касандру
    public static void WriteToKassandra(Session session,String key, Integer value){
        String cqlStatmentCassandra = "INSERT INTO java_api.logs_result (id,hour_priority, count) VALUES ( " + key.hashCode() + ", " + " ' "  + key + " ' " + ", " + value + " );";
         session.execute(cqlStatmentCassandra);
    }



}
