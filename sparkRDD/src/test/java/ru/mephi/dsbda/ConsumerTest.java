package ru.mephi.dsbda;


import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ConsumerTest {


    private static final String host = "localhost:9092";
    private static  String groupid = "htr";
    private static final String topic ="syslog1";

    @Test
    public void ConsumerGetTest() {
        ArrayList<String> logs = Consumer.consumer(host,groupid,topic);

        // получение публикаций в  топике
        int count = 37623;
        assertEquals(count,logs.size());


        // меняем groupid
        groupid = "uyt";
        logs = Consumer.consumer(host,groupid,topic);
        assertEquals(count,logs.size());

    }

    @Test
    public void RepeatReadTest(){
        ArrayList<String> logs = Consumer.consumer(host,groupid,topic);

        // повторное получение публикаций в  топике, с тем же group id
        int count = 0;
        assertEquals(count,logs.size());
    }



}
