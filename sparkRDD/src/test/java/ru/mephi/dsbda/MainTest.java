package ru.mephi.dsbda;


import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MainTest {

    private static final String EmptyTable = "Select count(*) from java_api.logs_result";
    private static final String SumTable = "Select sum(count) from java_api.logs_result";
    private static final String host = "localhost:9092";
    private static  final String groupid = "uuid";
    private static  final String topic = "syslog1";


    @Test(expected = Exception.class)
    public void args1() throws Exception {

        String[] arg = {host};
        Main.main(arg);
    }

    @Test(expected = Exception.class)
    public void args2() throws Exception {

        String[] arg = {host, groupid};
        Main.main(arg);
    }

    @Test
    public void EmptyTable(){
        Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
        Session session = cluster.connect("java_api");
        Object rezult = session.execute(EmptyTable).one().getToken(0).getValue();
        long i = 0,count = (long) rezult;
        assertEquals(i,rezult);
        session.close();
        cluster.close();
    }

    @Test
    public void Table () throws Exception {
        String[] arg = {host, groupid, topic};
        Main.main(arg);
        Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
        Session session = cluster.connect("java_api");
        Object rezult = session.execute(EmptyTable).one().getToken(0).getValue();
        long i = 78,count = (long) rezult;
        assertEquals(i,rezult);
        session.close();
        cluster.close();
    }



}
