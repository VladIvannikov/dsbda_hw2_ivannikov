package ru.mephi.dsbda;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class LogsWithCountsTest {


        private static final String log = "14 3";
        private static final Integer count = 53;

        @Test
        public void ConstructorTest() {
            LogsWithCounts logsWithCounts = new LogsWithCounts(log,count);
            assertNotNull(logsWithCounts);
            assertEquals("14 3",logsWithCounts.getLogs());
            assertEquals(count,logsWithCounts.getCount());
        }

    }

